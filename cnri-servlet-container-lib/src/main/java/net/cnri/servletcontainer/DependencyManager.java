package net.cnri.servletcontainer;

import java.util.function.Consumer;

public interface DependencyManager {

    <T> void require(String name, Class<T> klass, Consumer<T> consumer);

    <T> void provide(String name, Class<T> klass, T object);

    <T> void require(Class<T> klass, Consumer<T> consumer);

    <T> void provide(Class<T> klass, T object);

    void require(String name, Runnable runnable);

    void provide(String name);
}
