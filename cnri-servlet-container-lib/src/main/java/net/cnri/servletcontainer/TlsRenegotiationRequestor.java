package net.cnri.servletcontainer;

import javax.net.ssl.SSLException;
import javax.servlet.http.HttpServletRequest;
import java.security.cert.X509Certificate;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public interface TlsRenegotiationRequestor {
    boolean isNeedClientAuth();
    boolean isRequestSupportsTlsRenegotiation();
    void requestTlsRenegotiation(Boolean wantClientAuth, long timeout, TimeUnit unit) throws SSLException, TimeoutException;

    static boolean isWantingTlsRenegotiation(HttpServletRequest req, Boolean wantClientAuth, boolean force) {
        if (force) return true;
        if (wantClientAuth == null) return false;
        if (wantClientAuth.booleanValue()) {
            return extractCertificate(req) == null;
        } else {
            return extractCertificate(req) != null;
        }
    }

    static X509Certificate extractCertificate(HttpServletRequest request) {
        X509Certificate[] certs = (X509Certificate[]) request.getAttribute("javax.servlet.request.X509Certificate");
        if (null != certs && certs.length > 0) {
            return certs[0];
        }
        return null;
    }
}