package net.cnri.servletcontainer;

import net.cnri.servletcontainer.EmbeddedJettyConfig.ConnectorConfig;
import org.bouncycastle.util.io.pem.PemObject;
import org.bouncycastle.util.io.pem.PemReader;
import org.eclipse.jetty.alpn.server.ALPNServerConnectionFactory;
import org.eclipse.jetty.deploy.App;
import org.eclipse.jetty.deploy.DeploymentManager;
import org.eclipse.jetty.deploy.providers.WebAppProvider;
import org.eclipse.jetty.http.HttpVersion;
import org.eclipse.jetty.http2.server.HTTP2CServerConnectionFactory;
import org.eclipse.jetty.http2.server.HTTP2ServerConnectionFactory;
import org.eclipse.jetty.security.HashLoginService;
import org.eclipse.jetty.server.*;
import org.eclipse.jetty.server.handler.ContextHandler;
import org.eclipse.jetty.server.handler.ContextHandlerCollection;
import org.eclipse.jetty.server.handler.StatisticsHandler;
import org.eclipse.jetty.server.handler.gzip.GzipHandler;
import org.eclipse.jetty.util.ssl.AliasedX509ExtendedKeyManager;
import org.eclipse.jetty.util.ssl.SslContextFactory;
import org.eclipse.jetty.webapp.Configuration;
import org.eclipse.jetty.webapp.WebAppContext;
import org.eclipse.jetty.xml.XmlConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.Cipher;
import javax.crypto.EncryptedPrivateKeyInfo;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.net.ssl.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.security.KeyFactory;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.EncodedKeySpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class EmbeddedJetty {

    private static final Logger logger = LoggerFactory.getLogger(EmbeddedJetty.class);

    private Server httpServer;
    private ContextHandlerCollection contexts;
    private DeploymentManager priorityDeploymentManager;
    private DeploymentManager deploymentManager;
    private DependencyManagerImpl dependencyManager;
    private KeyFileWatcher keyFileWatcher;
    private final EmbeddedJettyConfig config;
    private File baseDir;
    private File webAppsBaseStorageDir;
    private File webAppsTempDir;
    private File webAppsDir;
    private File webAppsPriorityDir;
    private File jettyConfigFile;

    private static final SecureRandom random = new SecureRandom();
    public static final String[] HTTPS_KEY_STORE_FILE_NAMES = { "https.jks", "https.keystore", "https.key" };
    public static final String STARTUP_COMPLETE_EVENT = "priority-startup-complete";
    private Map<String, ConnectorConfig> configIdConnectorConfigMap = new HashMap<>();
    private Map<String, SslContextFactory> configIdSslContextFactoryMap = new HashMap<>();

    public EmbeddedJetty(EmbeddedJettyConfig config) {
        this.config = config;
        this.baseDir = config.getBaseDir();

        if (config.getWebAppsPath() != null) {
            webAppsDir = new File(config.getWebAppsPath());
        } else {
            webAppsDir = new File(baseDir, "webapps");
        }

        if (config.getWebAppsPriorityPath() != null) {
            webAppsPriorityDir = new File(config.getWebAppsPriorityPath());
        } else {
            webAppsPriorityDir = new File(baseDir, "webapps-priority");
        }

        if (config.getWebAppsTempPath() != null) {
            webAppsTempDir = new File(config.getWebAppsTempPath());
        } else {
            webAppsTempDir = new File(baseDir, "webapps-temp");
        }

        if (config.getWebAppsStoragePath() != null) {
            webAppsBaseStorageDir = new File(config.getWebAppsStoragePath());
        } else {
            webAppsBaseStorageDir = new File(baseDir, "webapps-storage");
        }

        if (config.getJettyXmlPath() != null) {
            jettyConfigFile = new File(config.getJettyXmlPath());
        } else {
            jettyConfigFile = new File(baseDir, "jetty.xml");
        }
    }

    public void setUpHttpServer() throws Exception {
        setupDependencyManager();
        keyFileWatcher = new KeyFileWatcher(this);
        if (config.isEnableDefaultHttpConfig()) {
            if (config.getConnectors().size() > 0) {
                httpServer = new Server();
                enableAnnotationConfiguration(httpServer);
                for (EmbeddedJettyConfig.ConnectorConfig connectorConfig : config.getConnectors()) {
                    addConnector(connectorConfig);
                }
                contexts = new ContextHandlerCollection();
                priorityDeploymentManager = setUpDeploymentManager(webAppsPriorityDir);
                if (priorityDeploymentManager != null) httpServer.addBean(priorityDeploymentManager, true);
                deploymentManager = setUpDeploymentManager(webAppsDir);
                File realmPropertiesFile = new File(baseDir, "realm.properties");
                if (realmPropertiesFile.exists()) {
                    HashLoginService loginService = new HashLoginService("default");
                    loginService.setConfig(realmPropertiesFile.getAbsolutePath());
                    httpServer.addBean(loginService);
                }
                for (Handler handler : config.getDefaultHandlers()) {
                    contexts.prependHandler(handler);
                }
                // apparently need StatisticsHandler for setStopTimeout (graceful shutdown)
                StatisticsHandler statsHandler = new StatisticsHandler();
                if (config.isEnableGzip()) {
                    // In Jetty 9 GzipFilter no longer useful; use this instead
                    GzipHandler gzipHandler = new GzipHandler();
                    gzipHandler.setMinGzipSize(config.getMinGzipSize());
                    gzipHandler.setIncludedMethods("GET", "POST", "PUT", "DELETE", "PATCH");
                    gzipHandler.setHandler(contexts);
                    statsHandler.setHandler(gzipHandler);
                } else {
                    statsHandler.setHandler(contexts);
                }
                httpServer.setHandler(statsHandler);
                httpServer.setStopTimeout(5000);
                httpServer.setStopAtShutdown(false);
            }
        } else {
            httpServer = new Server();
        }
        if (httpServer != null) {
            if (jettyConfigFile.exists()) {
                try (FileInputStream in = new FileInputStream(jettyConfigFile)) {
                    new XmlConfiguration(in).configure(httpServer);
                }
            }
        }
    }

    private static void enableAnnotationConfiguration(Server server) {
        Configuration.ClassList classlist = Configuration.ClassList.setServerDefault(server);
        classlist.addAfter("org.eclipse.jetty.webapp.FragmentConfiguration",
            "org.eclipse.jetty.plus.webapp.EnvConfiguration",
            "org.eclipse.jetty.plus.webapp.PlusConfiguration");
        classlist.addBefore("org.eclipse.jetty.webapp.JettyWebXmlConfiguration",
            "org.eclipse.jetty.annotations.AnnotationConfiguration");
    }

    private void setupDependencyManager() {
        dependencyManager = new DependencyManagerImpl();
        config.addContextAttribute(DependencyManager.class.getName(), dependencyManager);
        config.addSystemClass(DependencyManager.class.getName());
    }

    public void startPriorityDeploymentManager() throws Exception {
        if (priorityDeploymentManager != null) {
            priorityDeploymentManager.start();
        }
        dependencyManager.provide(STARTUP_COMPLETE_EVENT);
    }

    public void startHttpServer() throws Exception {
        if (httpServer != null) {
            httpServer.start();
            if (deploymentManager != null) {
                httpServer.addBean(deploymentManager, true);
                deploymentManager.start();
            }
            if (keyFileWatcher != null) keyFileWatcher.start();
            cleanUpOldTempDirs();
        }
    }

    private void cleanUpOldTempDirs() {
        Thread t = new Thread(this::cleanUpOldTempDirsSync, "EmbeddedJetty-cleanUpOldTempDirs");
        t.setDaemon(true);
        t.start();
    }

    private void cleanUpOldTempDirsSync() {
        try {
            String[] existingTempDirNames = webAppsTempDir.list();
            if (existingTempDirNames == null) return;
            Collection<File> deployedContextTempDirs = getDeployedContextTempDirs();
            for (String existingTempDirName : existingTempDirNames) {
                // try to delete only the expected temp dirs
                if (!existingTempDirName.startsWith("jetty-")) continue;
                if (!existingTempDirName.endsWith(".dir")) continue;
                File existingTempDir = new File(webAppsTempDir, existingTempDirName).getCanonicalFile();
                if (!deployedContextTempDirs.contains(existingTempDir)) {
                    deleteRecursively(existingTempDir.toPath());
                }
            }
        } catch (Exception e) {
            logger.warn("Error cleaning up old temp dirs on startup", e);
        }
    }

    public void join() throws InterruptedException {
        if (httpServer != null) httpServer.join();
    }

    public void stopHttpServer() {
        if (httpServer != null) {
            if (deploymentManager != null && priorityDeploymentManager != null) {
                deploymentManager.undeployAll();
            }
            if (keyFileWatcher != null) keyFileWatcher.stop();
            try {
                httpServer.stop();
                httpServer.join();
            } catch (Exception e) {
                e.printStackTrace();
            }
            httpServer = null;
        }
        try {
            deleteTempDirs();
        } catch (Exception e) {
            logger.warn("Error deleting temp dirs on shutdown", e);
        }
    }

    private void deleteTempDirs() throws IOException {
        for (File tempDir : getDeployedContextTempDirs()) {
            deleteRecursively(tempDir.toPath());
        }
    }

    private List<File> getDeployedContextTempDirs() throws IOException {
        if (contexts == null) return Collections.emptyList();
        Handler[] handlers = contexts.getChildHandlersByClass(WebAppContext.class);
        if (handlers == null) return Collections.emptyList();
        List<File> res = new ArrayList<>();
        for (Handler handler : handlers) {
            if (handler instanceof WebAppContext) {
                File tempDir = ((WebAppContext) handler).getTempDirectory().getCanonicalFile();
                if (tempDir.exists()) res.add(tempDir);
            }
        }
        return res;
    }

    private static void deleteRecursively(Path tempDirPath) throws IOException {
        Files.walkFileTree(tempDirPath, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                try {
                    Files.delete(file);
                } catch (NoSuchFileException e) {
                    // ignore
                }
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
                if (exc instanceof NoSuchFileException) return FileVisitResult.CONTINUE;
                return super.visitFileFailed(file, exc);
            }

            @Override
            public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                if (exc instanceof NoSuchFileException) return FileVisitResult.CONTINUE;
                if (exc != null) throw exc;
                try {
                    Files.delete(dir);
                } catch (NoSuchFileException e) {
                    // ignore
                }
                return FileVisitResult.CONTINUE;
            }
        });
    }

    @SuppressWarnings("resource")
    public void addConnector(ConnectorConfig connectorConfig) throws Exception {
        SslContextFactory sslContextFactory = buildSslContextFactory(connectorConfig);
        String configId = getConfigId(connectorConfig);
        configIdConnectorConfigMap.put(configId, connectorConfig);
        configIdSslContextFactoryMap.put(configId, sslContextFactory);
        HttpConfiguration httpConfig = new HttpConfiguration();
        httpConfig.setSendServerVersion(false);
        httpConfig.setRequestHeaderSize(8192);
        if (connectorConfig.getRedirectPort() > 0) {
            httpConfig.setSecureScheme("https");
            httpConfig.setSecurePort(connectorConfig.getRedirectPort());
        } else if (!connectorConfig.isHttps() && !connectorConfig.isHttpOnly()) {
            httpConfig.setSecureScheme("https");
            httpConfig.setSecurePort(connectorConfig.getPort());
        }
        boolean useHttp2 = connectorConfig.isHttp2() &&
            !"renegotiate".equalsIgnoreCase(connectorConfig.getHttpsClientAuth()) &&
            !System.getProperty("java.version").startsWith("1.8");
        HTTP2ServerConnectionFactory http2 = null;
        ALPNServerConnectionFactory alpn = null;
        SslConnectionFactory ssl = null;
        if (!connectorConfig.isHttpOnly()) {
            SecureRequestCustomizer secureRequestCustomizer = new SecureRequestCustomizer();
            httpConfig.addCustomizer(secureRequestCustomizer);
            httpConfig.addCustomizer(new TlsRenegotiationEnablingCustomizer(secureRequestCustomizer));
            String protocol = HttpVersion.HTTP_1_1.asString();
            if (useHttp2) {
                http2 = new HTTP2ServerConnectionFactory(httpConfig);
                alpn = new ALPNServerConnectionFactory();
                alpn.setDefaultProtocol(HttpVersion.HTTP_1_1.asString());
                protocol = alpn.getProtocol();
            }
            ssl = new SslConnectionFactory(sslContextFactory, protocol);
        }
        HttpConnectionFactory http = new HttpConnectionFactory(httpConfig);
        HTTP2CServerConnectionFactory http2C = null;
        if (useHttp2) {
            http2C = new HTTP2CServerConnectionFactory(httpConfig);
        }
        ServerConnector connector;
        if (useHttp2) {
            if (connectorConfig.isHttps()) {
                connector = new ServerConnector(httpServer, ssl, alpn, http2, http, http2C);
            } else if (connectorConfig.isHttpOnly()) {
                connector = new ServerConnector(httpServer, http, http2C);
            } else {
                connector = new ServerConnector(httpServer, new OptionalSslConnectionFactory(ssl, HttpVersion.HTTP_1_1.asString()), ssl, alpn, http2, http, http2C);
            }
        } else {
            if (connectorConfig.isHttps()) {
                connector = new ServerConnector(httpServer, ssl, http);
            } else if (connectorConfig.isHttpOnly()) {
                connector = new ServerConnector(httpServer, http);
            } else {
                connector = new ServerConnector(httpServer, new OptionalSslConnectionFactory(ssl, HttpVersion.HTTP_1_1.asString()), ssl, http);
            }
        }
        if (!connectorConfig.isHttpOnly()) {
            List<String> filesToWatch =
                    Stream.of(connectorConfig.getHttpsKeyStoreFile(), connectorConfig.getHttpsPrivKeyFile(),
                              connectorConfig.getHttpsCertificateChainFile())
                    .filter(s -> s != null && !s.trim().isEmpty())
                    .collect(Collectors.toList());
            if (filesToWatch.size() > 0) {
                for (String file : filesToWatch) {
                    if (file == null || file.trim().isEmpty()) continue;
                    Path path = new File(file).toPath();
                    keyFileWatcher.addFileListener(path, configId);
                    logger.info("Watching key file for changes: " + file);
                }
            }
        }
        connector.setPort(connectorConfig.getPort());
        InetAddress httpsListenAddress = connectorConfig.getListenAddress();
        if (httpsListenAddress != null) connector.setHost(httpsListenAddress.getHostAddress());
        connector.setIdleTimeout(30000);
        httpServer.addConnector(connector);
    }

    private String getConfigId(ConnectorConfig connectorConfig) {
        String address;
        if (connectorConfig.getListenAddress() == null) {
            address = "0.0.0.0";
        } else {
            address = connectorConfig.getListenAddress().getHostAddress();
        }
        // Hardcode TCP protocol for now. UDP might be needed in the future, when HTTP3 (which is over UDP) lands.
        return "tcp:" + address + ":" + connectorConfig.getPort();
    }

    private SslContextFactory buildSslContextFactory(ConnectorConfig connectorConfig) throws Exception {
        if (connectorConfig.isHttpOnly()) return null;
        String ephemeralDHKeySize = System.getProperty("jdk.tls.ephemeralDHKeySize");
        if (ephemeralDHKeySize == null) {
            System.setProperty("jdk.tls.ephemeralDHKeySize", "2048");
        }
        KeyManager[] keyManagers;
        if (connectorConfig.isHttpsUseSelfSignedCert() || (connectorConfig.getHttpsKeyStoreFile() == null && connectorConfig.getHttpsPrivKeyFile() == null)) {
            if (connectorConfig.getHttpsCertificateChain() != null) {
                keyManagers = new KeyManager[] { new AutoSelfSignedKeyManager(connectorConfig.getHttpsId(), connectorConfig.getHttpsCertificateChain(), connectorConfig.getHttpsPrivKey()) };
            } else if (connectorConfig.getHttpsPubKey() != null && connectorConfig.getHttpsPrivKey() != null) {
                keyManagers = new KeyManager[] { new AutoSelfSignedKeyManager(connectorConfig.getHttpsId(), connectorConfig.getHttpsPubKey(), connectorConfig.getHttpsPrivKey()) };
            } else {
                keyManagers = new KeyManager[] { new AutoSelfSignedKeyManager(connectorConfig.getHttpsId()) };
            }
        } else {
            keyManagers = loadKeyManagersFromFile(connectorConfig);
        }
        SSLContext sslContext = SSLContext.getInstance("TLS");
        sslContext.init(keyManagers, new TrustManager[] { new AllTrustingTrustManager() }, random);
        SslContextFactory sslContextFactory = new SslContextFactory();
        String clientAuth = connectorConfig.getHttpsClientAuth();
        sslContextFactory.setWantClientAuth(true);
        if ("need".equalsIgnoreCase(clientAuth) || "true".equalsIgnoreCase(clientAuth)) sslContextFactory.setNeedClientAuth(true);
        // else if("want".equalsIgnoreCase(clientAuth)) sslContextFactory.setWantClientAuth(true);
        else if (clientAuth == null || "false".equalsIgnoreCase(clientAuth) || "renegotiate".equalsIgnoreCase(clientAuth)) sslContextFactory.setWantClientAuth(false);
        sslContextFactory.setSslContext(sslContext);
        sslContextFactory.setIncludeCipherSuites(TlsProtocolAndCipherSuiteConfigurationUtil.ENABLED_CIPHER_SUITES);
        sslContextFactory.setUseCipherSuitesOrder(true);
        // Currently only support TLSv1.2 in order for TlsRenegotiationRequestor to work
        if ("renegotiate".equalsIgnoreCase(clientAuth)) {
            sslContextFactory.setIncludeProtocols(TlsProtocolAndCipherSuiteConfigurationUtil.ENABLED_PROTOCOLS_WITH_RENEGOTIATION);
        } else {
            sslContextFactory.setIncludeProtocols(TlsProtocolAndCipherSuiteConfigurationUtil.ENABLED_PROTOCOLS);
        }
        sslContextFactory.setEndpointIdentificationAlgorithm(null);
        return sslContextFactory;
    }

    public void reloadKeyManager(String configId) throws Exception {
        SslContextFactory sslContextFactory = configIdSslContextFactoryMap.get(configId);
        ConnectorConfig connectorConfig = configIdConnectorConfigMap.get(configId);
        SSLContext sslContext = SSLContext.getInstance("TLS");
        KeyManager[] keyManagers = loadKeyManagersFromFile(connectorConfig);
        sslContext.init(keyManagers, new TrustManager[]{new AllTrustingTrustManager()}, random);
        sslContextFactory.setSslContext(sslContext);
        sslContextFactory.reload(s -> logger.info("Reloading EmbeddedJetty keys"));
    }

    private KeyManager[] loadKeyManagersFromFile(ConnectorConfig connectorConfig) throws Exception {
        String keyPassStr = connectorConfig.getHttpsKeyPassword();
        char[] keyPass = keyPassStr == null ? new char[0] : keyPassStr.toCharArray();
        if (connectorConfig.getHttpsKeyStoreFile() != null) {
            KeyStore httpsKeyStore = KeyStore.getInstance("JKS");
            String keystorePassStr = connectorConfig.getHttpsKeyStorePassword();
            char[] keystorePass = keystorePassStr == null ? null : keystorePassStr.toCharArray();
            File keystoreFile = getKeystoreFile(connectorConfig.getHttpsKeyStoreFile());
            if (keystoreFile == null) {
                throw new CertificateException("Certificate file specified does not exist");
            }
            try (FileInputStream in = new FileInputStream(keystoreFile)) {
                httpsKeyStore.load(in, keystorePass);
            }
            KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
            kmf.init(httpsKeyStore, keyPass);
            return getKeyManagers(kmf, connectorConfig.getHttpsAlias());
        } else {
            File privateKeyFile = new File(connectorConfig.getHttpsPrivKeyFile());
            PrivateKey key = readPrivateKeyFromFile(privateKeyFile, keyPassStr);

            if (connectorConfig.getHttpsCertificateChainFile() == null) {
                throw new CertificateException("Certificate file setting is required when using HttpsPrivateKeyFile");
            }
            File certificateChainFile = new File(connectorConfig.getHttpsCertificateChainFile());
            List<X509Certificate> certificateChain = readCertificateChain(certificateChainFile);
            if (certificateChain.isEmpty()) {
                throw new CertificateException("Certificate file does not contain any certificates: " + certificateChainFile);
            }

            return new KeyManager[] { new AutoSelfSignedKeyManager(connectorConfig.getHttpsId(), certificateChain.toArray(new X509Certificate[0]), key) };
        }
    }

    private PrivateKey readPrivateKeyFromFile(File privateKeyFile, String password) throws Exception {
        try (PemReader pemReader = new PemReader(Files.newBufferedReader(privateKeyFile.toPath()))) {
            PemObject pemObject = pemReader.readPemObject();
            byte[] content = pemObject.getContent();
            EncodedKeySpec keySpec;
            if (!pemObject.getType().contains("ENCRYPTED")) {
                keySpec = new PKCS8EncodedKeySpec(content);
            } else {
                if (password == null) {
                    throw new Exception("Password required for encrypted key file " + privateKeyFile);
                }
                EncryptedPrivateKeyInfo encryptedPrivateKeyInfo = new EncryptedPrivateKeyInfo(content);
                SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(encryptedPrivateKeyInfo.getAlgName());
                SecretKey secretKey = keyFactory.generateSecret(new PBEKeySpec(password.toCharArray()));
                Cipher cipher = Cipher.getInstance(encryptedPrivateKeyInfo.getAlgName());
                cipher.init(Cipher.DECRYPT_MODE, secretKey, encryptedPrivateKeyInfo.getAlgParameters());
                keySpec = encryptedPrivateKeyInfo.getKeySpec(cipher);
            }
            try {
                KeyFactory keyFactory = KeyFactory.getInstance("RSA");
                return keyFactory.generatePrivate(keySpec);
            } catch (InvalidKeySpecException ignore) {
                KeyFactory keyFactory = KeyFactory.getInstance("DSA");
                return keyFactory.generatePrivate(keySpec);
            }
        }
    }

    private List<X509Certificate> readCertificateChain(File certificateChainFile) throws Exception {
        CertificateFactory cf = CertificateFactory.getInstance("X.509");
        try (FileInputStream fis = new FileInputStream(certificateChainFile)) {
            return cf.generateCertificates(fis)
                    .stream()
                    .map(c -> (X509Certificate) c)
                    .collect(Collectors.toList());
        }
    }

    public void addWebApp(File war, String contextPath) throws Exception {
        WebAppContext handler = new WebAppContext(war.getAbsolutePath(), contextPath);
        setUpWebAppContext(handler);
        for (String attributeName : config.getContextAttributes().keySet()) {
            handler.setAttribute(attributeName, config.getContextAttributes().get(attributeName));
        }
        handler.setExtractWAR(true);
        handler.setAttribute(WebAppContext.BASETEMPDIR, webAppsTempDir);
        contexts.prependHandler(handler);
        handler.start();
    }

    private DeploymentManager setUpDeploymentManager(File webAppsDir) {
        // return handlers in reverse order in order to allow later entry to override
        //File webAppsDir = new File(baseDir, filename);
        //if((webAppsDir.exists() && webAppsDir.isDirectory()) || webAppsDir.mkdirs()) {
        if (!webAppsDir.exists() || webAppsDir.isDirectory()) {
            WebAppProvider webAppProvider = new EmbeddedWebAppProvider();
            // This is the Jetty default descriptor, minus the TRACE disabling security constraint.
            webAppProvider.setDefaultsDescriptor("/webdefault.xml");
            webAppProvider.setMonitoredDirName(webAppsDir.getAbsolutePath());
            webAppProvider.setParentLoaderPriority(false);
            webAppProvider.setExtractWars(true);
            webAppProvider.setTempDir(webAppsTempDir);
            webAppProvider.setScanInterval(10);
            DeploymentManager result = new DeploymentManager();
            result.setContexts(contexts);
            for (String attributeName : config.getContextAttributes().keySet()) {
                result.setContextAttribute(attributeName, config.getContextAttributes().get(attributeName));
            }
            result.addAppProvider(webAppProvider);
            return result;
        } else {
            return null;
        }
    }

    private static KeyManager[] getKeyManagers(KeyManagerFactory kmf, String alias) throws Exception {
        KeyManager[] res = kmf.getKeyManagers();
        if (alias == null || res == null) return res;
        for (int i = 0; i < res.length; i++) {
            if (res[i] instanceof X509KeyManager) res[i] = new AliasedX509ExtendedKeyManager((X509ExtendedKeyManager) (res[i]), alias);
        }
        return res;
    }

    private File getKeystoreFile(String filename) {
        if (filename != null) {
            File res = new File(filename);
            if (res.isAbsolute()) return res;
            else return new File(baseDir, filename);
        } else {
            for (String name : HTTPS_KEY_STORE_FILE_NAMES) {
                File res = new File(baseDir, name);
                if (res.exists()) return res;
            }
            return null;
        }
    }

    private class EmbeddedWebAppProvider extends WebAppProvider {
        @Override
        public ContextHandler createContextHandler(App app) throws Exception {
            getTempDir().mkdirs();
            WebAppContext res = (WebAppContext) super.createContextHandler(app);
            File webAppStorageDir = getWebAppStorageDirForContextPath(res.getContextPath());
            res.setAttribute("net.cnri.servercontainer.webapp_storage_directory", webAppStorageDir);
            setUpWebAppContext(res);
            return res;
        }
    }

    private File getWebAppStorageDirForContextPath(String contextPath) {
        if (contextPath == null) return null;
        File result = null;
        String webAppName;
        if ("/".equals(contextPath)) {
            webAppName = "root";
        } else if (contextPath.startsWith("/")) {
            webAppName = contextPath.substring(1);
        } else {
            webAppName = contextPath;
        }
        result = new File(webAppsBaseStorageDir, webAppName);
        result.mkdirs();
        return result;
    }

    private void setUpWebAppContext(WebAppContext res) {
        for (String systemClass : config.getSystemClasses()) {
            res.getSystemClasspathPattern().add(systemClass);
        }
        for (String serverClass : config.getServerClasses()) {
            res.getServerClasspathPattern().add(serverClass);
        }
        res.setExtraClasspath(config.getExtraClasspath());
        // res.setAllowNullPathInfo(true);
        res.setInitParameter("org.eclipse.jetty.servlet.Default.dirAllowed", "false");
    }
}
