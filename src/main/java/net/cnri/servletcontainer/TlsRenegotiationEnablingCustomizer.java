package net.cnri.servletcontainer;

import org.eclipse.jetty.io.EndPoint;
import org.eclipse.jetty.io.ssl.SslConnection;
import org.eclipse.jetty.io.ssl.SslConnection.DecryptedEndPoint;
import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.HttpConfiguration;
import org.eclipse.jetty.server.HttpConfiguration.Customizer;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.SecureRequestCustomizer;

public class TlsRenegotiationEnablingCustomizer implements Customizer {

    private final SecureRequestCustomizer secureRequestCustomizer;

    public TlsRenegotiationEnablingCustomizer(SecureRequestCustomizer secureRequestCustomizer) {
        this.secureRequestCustomizer = secureRequestCustomizer;
    }

    @Override
    public void customize(Connector connector, HttpConfiguration config, Request request) {
        @SuppressWarnings("resource")
        EndPoint endPoint = request.getHttpChannel().getEndPoint();
        if (endPoint instanceof DecryptedEndPoint) {
            DecryptedEndPoint decryptedEndPoint = (DecryptedEndPoint) endPoint;
            @SuppressWarnings("resource")
            SslConnection sslConnection = decryptedEndPoint.getSslConnection();
            // The point of this Customizer is to add this attribute
            request.setAttribute(TlsRenegotiationRequestor.class.getName(), new TlsRenegotiationRequestorImpl(sslConnection, request, secureRequestCustomizer, connector, config));
        }
    }
}
