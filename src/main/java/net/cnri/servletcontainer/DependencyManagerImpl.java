package net.cnri.servletcontainer;

import java.util.*;
import java.util.function.Consumer;

public class DependencyManagerImpl implements DependencyManager {

    private final Map<Map.Entry<String, Class<?>>, Object> providedObjects = new HashMap<>();
    private final Map<Map.Entry<String, Class<?>>, List<Consumer<Object>>> requireCallbacks = new HashMap<>();

    @Override
    @SuppressWarnings("unchecked")
    public synchronized <T> void require(String name, Class<T> klass, Consumer<T> consumer) {
        Map.Entry<String, Class<?>> key = new AbstractMap.SimpleEntry<>(name, klass);
        if (providedObjects.containsKey(key)) {
            T obj = (T) providedObjects.get(key);
            consumer.accept(obj);
            return;
        }
        requireCallbacks.computeIfAbsent(key, x -> new ArrayList<>()).add((Consumer<Object>) consumer);
    }

    @Override
    public synchronized <T> void provide(String name, Class<T> klass, T object) {
        Map.Entry<String, Class<?>> key = new AbstractMap.SimpleEntry<>(name, klass);
        providedObjects.put(key, object);
        if (!requireCallbacks.containsKey(key)) return;
        for (Consumer<Object> consumer : requireCallbacks.get(key)) {
            consumer.accept(object);
        }
        requireCallbacks.remove(key);
    }

    @Override
    public <T> void require(Class<T> klass, Consumer<T> consumer) {
        require(null, klass, consumer);
    }

    @Override
    public <T> void provide(Class<T> klass, T object) {
        provide(null, klass, object);
    }

    @Override
    public void require(String name, Runnable runnable) {
        require(name, null, unused -> runnable.run());
    }
    @Override

    public void provide(String name) {
        provide(name, null, null);
    }
}
