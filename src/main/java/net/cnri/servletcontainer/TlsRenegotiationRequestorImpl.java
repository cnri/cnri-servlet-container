package net.cnri.servletcontainer;

import org.eclipse.jetty.io.ssl.SslConnection;
import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.HttpConfiguration;
import org.eclipse.jetty.server.HttpConnection;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.SecureRequestCustomizer;
import org.eclipse.jetty.util.BufferUtil;
import org.eclipse.jetty.util.Callback;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.net.ssl.SSLEngine;
import javax.net.ssl.SSLEngineResult.HandshakeStatus;
import javax.net.ssl.SSLException;

// Note: this technique works for TLS 1.2 and lower.
// TLS 1.3 does not have renegotiation but has a separate approach to
// "Post-Handshake Client Authentication"; that requires new code once there is Java support.
// HTTP/2 also forbids renegotiation even over TLS 1.2.
public class TlsRenegotiationRequestorImpl implements TlsRenegotiationRequestor {
    private final SslConnection sslConnection;
    private final Request request;
    private final SecureRequestCustomizer secureRequestCustomizer;
    private final Connector connector;
    private final HttpConfiguration config;

    public TlsRenegotiationRequestorImpl(SslConnection sslConnection, Request request, SecureRequestCustomizer secureRequestCustomizer, Connector connector, HttpConfiguration config) {
        this.sslConnection = sslConnection;
        this.request = request;
        this.secureRequestCustomizer = secureRequestCustomizer;
        this.connector = connector;
        this.config = config;
    }

    @Override
    public boolean isNeedClientAuth() {
        SSLEngine sslEngine = sslConnection.getSSLEngine();
        return sslEngine.getNeedClientAuth();
    }

    @Override
    public boolean isRequestSupportsTlsRenegotiation() {
        SSLEngine sslEngine = sslConnection.getSSLEngine();
        if (sslEngine.getSession().getProtocol().compareTo("TLSv1.3") >= 0) return false;
        if (!(sslConnection.getDecryptedEndPoint().getConnection() instanceof HttpConnection)) return false;
        return true;
    }

    @Override
    public void requestTlsRenegotiation(Boolean wantClientAuth, long timeout, TimeUnit unit) throws SSLException, TimeoutException {
        if (!isRequestSupportsTlsRenegotiation()) {
            throw new SSLException("Renegotiation requires using HTTP/1.1 over TLS 1.2");
        }
        SSLEngine sslEngine = sslConnection.getSSLEngine();
        // We need to invalidate the old session in order to prevent session resumption
        sslEngine.getSession().invalidate();
        // don't downgrade from need-client-auth, which only occurs if explicitly configured that way
        if (wantClientAuth != null && !sslEngine.getNeedClientAuth()) {
            sslEngine.setWantClientAuth(wantClientAuth.booleanValue());
        }
        // begin handshake
        sslEngine.beginHandshake();
        // activate the handshake
        try {
            waitForHandshake(null).get(timeout, unit);
        } catch (InterruptedException e) {
            throw new SSLException("TLS renegotiation interrupted", e);
        } catch (ExecutionException | CompletionException e) {
            Throwable t = e;
            while (t instanceof CompletionException || t instanceof ExecutionException) {
                t = t.getCause();
            }
            if (t instanceof RuntimeException) throw (RuntimeException) t;
            if (t instanceof Error) throw (Error) t;
            if (t instanceof TimeoutException) throw (TimeoutException) t;
            if (t instanceof SSLException) throw (SSLException) t;
            throw new SSLException("Unexpected exception in renegotiation handshake", t);
        }
        // fix client certificate etc. attributes
        request.removeAttribute("javax.servlet.request.X509Certificate");
        secureRequestCustomizer.customize(connector, config, request);
    }

    private CompletableFuture<Void> waitForHandshake(@SuppressWarnings("unused") Void ignored) {
        if (sslConnection.getSSLEngine().getHandshakeStatus() == HandshakeStatus.NOT_HANDSHAKING) {
            return CompletableFuture.completedFuture(null);
        }
        Callback.Completable promise = new Callback.Completable();
        sslConnection.getDecryptedEndPoint().write(promise, BufferUtil.EMPTY_BUFFER);
        return promise.thenCompose(this::waitForHandshake);
    }
}
