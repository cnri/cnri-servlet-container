package net.cnri.servletcontainer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.FileTime;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class KeyFileWatcher {

    private static final Logger logger = LoggerFactory.getLogger(KeyFileWatcher.class);
    private final EmbeddedJetty embeddedJetty;
    private Map<Path, Set<String>> pathConfigIdMap;
    private Map<Path, FileTime> lastModifiedMap;
    private ScheduledExecutorService execServ;
    private boolean started = false;

    public KeyFileWatcher(EmbeddedJetty embeddedJetty) {
        this.embeddedJetty = embeddedJetty;
        pathConfigIdMap = new HashMap<>();
        lastModifiedMap = new HashMap<>();
    }

    public synchronized void start() {
        if (started) throw new IllegalStateException("KeyFileWatcher already started");
        started = true;
        if (lastModifiedMap.isEmpty()) return;
        execServ = Executors.newSingleThreadScheduledExecutor();
        execServ.scheduleAtFixedRate(this::checkForChanges, 0, 15, TimeUnit.SECONDS);
    }

    private void checkForChanges() {
        try {
            Set<String> configsToReload = new HashSet<>();
            for (Map.Entry<Path, FileTime> entry : lastModifiedMap.entrySet()) {
                Path path = entry.getKey();
                FileTime oldLastModified = entry.getValue();
                FileTime newLastModified;
                boolean exists = Files.exists(path);
                boolean reload;
                if (exists) {
                    newLastModified = Files.getLastModifiedTime(path);
                    reload = newLastModified.compareTo(oldLastModified) > 0;
                } else {
                    newLastModified = FileTime.fromMillis(0);
                    reload = oldLastModified.toMillis() != 0;
                }
                if (reload) {
                    configsToReload.addAll(pathConfigIdMap.get(path));
                    lastModifiedMap.put(path, newLastModified);
                    logger.info("Saw key file change: " + path.toFile().getAbsolutePath());
                }
            }
            for (String configId : configsToReload) {
                embeddedJetty.reloadKeyManager(configId);
            }
        } catch (Exception e) {
            logger.error("Error in KeyFileWatcher", e);
        }
    }

    public synchronized void stop() {
        if (execServ == null) return;
        execServ.shutdown();
        try {
            execServ.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    public synchronized void addFileListener(Path path, String configId) throws IOException {
        if (started) throw new IllegalStateException("KeyFileWatcher already started");
        pathConfigIdMap.computeIfAbsent(path, k -> new HashSet<>()).add(configId);
        if (Files.exists(path)) {
            FileTime lastModified = Files.getLastModifiedTime(path);
            lastModifiedMap.put(path, lastModified);
        } else {
            lastModifiedMap.put(path, FileTime.fromMillis(0));
        }
    }
}
